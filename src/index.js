import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';

import App from './components/app';
import ErrorBoundry from './components/error-boundry';

import './styles/app.scss';

ReactDOM.render(
    <ErrorBoundry>
        <Router>
            <App />
        </Router>
    </ErrorBoundry>,
    document.getElementById('root')
);
