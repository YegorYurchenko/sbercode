import React from 'react';

const Footer = () => (
    <footer className="footer">
        <div className="footer__inner">
            <p className="footer__text">© 1997–2020 ПАО Сбербанк</p>
            <p className="footer__text">Россия, Москва, 117997, ул. Вавилова 19. Генеральная лицензия на осуществление банковских операций от 11 августа 2015 года.</p>
            <p className="footer__text">Регистрационный номер — 1481.</p>
        </div>
    </footer>
);

export default Footer;
