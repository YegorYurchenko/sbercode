import React from 'react';
import axios from 'axios';

const agreeBtn = React.createRef();
const refuseBtn = React.createRef();

const userDataAgreement = (answer, phoneNumber, returnUrl, tempToken) => {
    window.location.href = returnUrl;
    // axios({
    //     method: 'post',
    //     url: 'https://sberid.odigital.site/sendAgreement',
    //     data: { userDataAgreement: answer, phoneNumber, returnUrl, tempToken },
    // })
    //     .then(() => window.location.href = returnUrl)
    //     .catch((e) => {
    //         // eslint-disable-next-line
    //         console.error(e);
    //     });
};

const PopupDataAgreement = ({ isSberUser, phoneNumber, returnUrl, tempToken }) => {
    let popupStyle = 'popup-data-agreement';
    if (isSberUser) popupStyle += ' is-active';

    return (
        <div className={popupStyle}>
            <div className="popup-data-agreement__inner">
                <p className="popup-data-agreement__text">
                    Мы видим, что вы являетесь Клиентом экосистемы Сбербанка. Вы разрешаете нам поделиться Вашей контактной информацией (ФИО, Email) с сервисом?
                </p>
                <p className="popup-data-agreement__text">
                    Для детального просмотра политики в отношении обработки персональных данных нажмите 
                    <a className="popup-data-agreement__text-link" href="https://yandex.ru" 
                        target="_blank" rel="noopener noreferrer">
                        здесь
                    </a>
                </p>

                <div className="popup-data-agreement__btns-wrap">
                    <button className="popup-data-agreement__btn popup-data-agreement__btn-agree" 
                        type="button"
                        onClick={() => userDataAgreement(true, phoneNumber, returnUrl, tempToken)}
                        ref={agreeBtn}
                    >
                        Да
                    </button>
                    <button className="popup-data-agreement__btn" 
                        type="button"
                        onClick={() => userDataAgreement(false, phoneNumber, returnUrl, tempToken)}
                        ref={refuseBtn}
                    >
                        Нет
                    </button>
                </div>
            </div>
        </div>
    );
};

export default PopupDataAgreement;
