import React from 'react';
import sberIcon from '../../images/icon-sber.svg';

const Header = () => (
    <header className="header">
        <h1 className="header__sber-icon-wrap">
            <img className="header__sber-icon" src={sberIcon} alt="Сбербанк ID" />
        </h1>
        <div className="header__link-wrap">
            <a className="header__link" href="https://www.sberbank.ru/ru/person/dist_services/sberbankid">
                <span className="header__link-text">Что такое Сбербанк ID?</span>
                <svg viewBox='0 0 129 129' className="header__link-svg">
                    <g><path d='m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z'/></g>
                </svg>
            </a>
            <a className="header__link-mobile" href="https://www.sberbank.ru/ru/person/dist_services/sberbankid">
                <span className="header__link-text">Подробнее</span>
                <svg viewBox='0 0 129 129' className="header__link-svg">
                    <g><path d='m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z'/></g>
                </svg>
            </a>
        </div>
    </header>
);

export default Header;
