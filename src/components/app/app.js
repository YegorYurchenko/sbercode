import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from '../header';
import Footer from '../footer';
import ErrorIndicator from '../error-indicator';
import SberId from '../../pages';

const App = () => (
    <React.Fragment>
        <Header />
        <main role="main" className="app">
            <Switch>
                <Route path="/sber-id:options"
                    render={({ location }) => {
                        const { pathname, search } = location;
                        const options = /&returnUrl=(.*)&tempToken=([0-9abcdef]*)/g.exec(pathname + search);

                        let returnUrl, tempToken;
                        if (options) {
                            [returnUrl, tempToken] = options.slice(1,3);
                            return <SberId returnUrl={decodeURI(returnUrl)} tempToken={tempToken} />;
                        } else {
                            return <ErrorIndicator />;
                        }
                    }}
                />
                
                <ErrorIndicator />;
            </Switch>
        </main>
        <div className="sber-id__create-wrap is-mobile">
            <div className="sber-id__create">
                <a className="sber-id__create-link" href="https://online.sberbank.ru/CSAFront/oidc/authorize.do?response_type=code&scope=openid+name+maindoc+email+inn+snils+mobile+international_passport+address_reg+work_address+address_of_actual_residence&client_id=b09060f3-2acb-41a7-8818-9edd33cbfe92&state=rj1p2hhj14qs6w2j7aw5h&nonce=y0in0sjq56mh36hva6woqi&redirect_uri=https://id.sberbank.ru/&_ga=2.79119773.808891086.1596106033-1576560545.1596106033#/registration">
                    <span className="sber-id__create-link-text">Создать Сбербанк ID</span>
                    <svg viewBox='0 0 129 129' className="sber-id__create-link-svg">
                        <g><path d='m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z'/></g>
                    </svg>
                </a>
                <p className="sber-id__create-desc">
                    Сбербанк ID - единый вход в сервисы Сбербанка и партнеров
                </p>
            </div>
        </div>
        <Footer />
    </React.Fragment>
);

export default App;
