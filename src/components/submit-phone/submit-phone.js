import React from 'react';
import axios from 'axios';
import Spinner from '../spinner';

const phoneInput = React.createRef();
const phoneSubmit = React.createRef();
const phoneSubmitLoading = React.createRef();

const correctPhone = (phoneNumber) => {
    phoneNumber = phoneNumber.trim();

    if (phoneNumber.length < 4 || !phoneNumber[3].match(/\d/)) {
        phoneInput.current.value = '+7 ';
        return '';
    }

    phoneNumber = phoneNumber.split('').map((number, idx) => {
        if (idx > 2) {
            if (!number.match(/\d/)) {
                return '';
            } else if (idx > 14) {
                return '';
            }

            return number;
        }

        return number;
    }).join('');

    phoneNumber = `${phoneNumber.slice(0,6)} ${phoneNumber.slice(6,9)} ${phoneNumber.slice(9)}`
    phoneInput.current.value = phoneNumber.trim();

    if (phoneNumber.length === 15) {
        phoneSubmit.current.classList.add('is-active');
    } else {
        phoneSubmit.current.classList.remove('is-active');
    }
};

const submitPhone = (submitPhoneStatus, returnUrl, tempToken) => {
    const phoneNumber = phoneInput.current.value.slice(1).replace(/ /g, '');
    const data = { phoneNumber, returnUrl, tempToken };

    phoneInput.current.classList.remove('is-error');
    phoneSubmit.current.classList.add('is-hidden');
    phoneSubmitLoading.current.classList.remove('is-hidden');

    submitPhoneStatus(true, phoneNumber);

    // axios({
    //     method: 'post',
    //     url: 'https://sberid.odigital.site/getCode',
    //     data,
    // })
    //     .then((response) => {
    //         const { data, status } = response.data;

    //         switch (status) {
    //             case 'SUBMIT_PHONE_NUMBER_SUCCESS':
    //                 submitPhoneStatus(data.submitPhone, phoneNumber);
    //                 break;
    //             case 'SUBMIT_PHONE_NUMBER_FAIL':
    //                 submitPhoneStatus(data.submitPhone);

    //                 try {
    //                     phoneInput.current.classList.add('is-error');
    //                     phoneSubmit.current.classList.remove('is-hidden');
    //                     phoneSubmitLoading.current.classList.add('is-hidden');
    //                 } catch {
    //                     // eslint-disable-next-line
    //                     console.error('Ошибка');
    //                 }
    //                 break;
    //             default:
    //                 // eslint-disable-next-line
    //                 console.error('Ошибка');
    //                 break;
    //         }
    //     })
    //     .catch((e) => {
    //         // eslint-disable-next-line
    //         console.error(e);

    //         try {
    //             phoneInput.current.classList.add('is-error');
    //             phoneSubmit.current.classList.remove('is-hidden');
    //             phoneSubmitLoading.current.classList.add('is-hidden');
    //         } catch {
    //             // eslint-disable-next-line
    //             console.error('Ошибка');
    //         }
    //     });
};

const SubmitPhone = ({ submitPhoneStatus, returnUrl, tempToken }) => (
    <div className="submit-phone">
        <p className="submit-phone__desc">
            Введите номер телефона, чтобы использовать Сбербанк ID для входа на ресурс:
        </p>
        <form className="submit-phone__form" name="submitPhone">
            <input 
                type="text" 
                className="submit-phone__input-phone" 
                placeholder="+7 ___ ___ ____"
                onChange={(e) => correctPhone(e.target.value)}
                onFocus={(e) => correctPhone(e.target.value)}
                ref={phoneInput}
            />
            <span className="submit-phone__error">Что-то пошло не так, попробуйте ещё раз</span>
            <button 
                type="button" 
                className="submit-phone__submit"
                onClick={() => submitPhone(submitPhoneStatus, returnUrl, tempToken)}
                ref={phoneSubmit}>
                Отправить
            </button>
            <button 
                type="button" 
                className="submit-phone__submit is-active is-hidden"
                ref={phoneSubmitLoading}>
                <Spinner />
            </button>
        </form>
    </div>
);

export default SubmitPhone;