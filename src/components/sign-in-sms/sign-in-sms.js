import React from 'react';
import axios from 'axios';
import Spinner from '../spinner';

const smsInput = React.createRef();
const smsSuccess = React.createRef();
const smsSubmit = React.createRef();
const smsSubmitLoading = React.createRef();

const correctSmsCode = (smsCode) => {
    if (smsCode.length && !smsCode[smsCode.length - 1].match(/\d/)) {
        smsInput.current.value = smsCode.slice(0, smsCode.length - 1);
        return '';
    }

    smsInput.current.value = smsCode;

    if (smsCode.length > 0) {
        smsSubmit.current.classList.add('is-active');
    } else {
        smsSubmit.current.classList.remove('is-active');
    }
};

const submitSmsCode = (phoneNumber, getDataSberUser, returnUrl, tempToken) => {
    const data = { 
        smsCode: +smsInput.current.value,
        phoneNumber,
        returnUrl,
        tempToken,
    };
    
    smsInput.current.classList.remove('is-error');
    smsSubmit.current.classList.add('is-hidden');
    smsSubmitLoading.current.classList.remove('is-hidden');

    getDataSberUser(true);

    // axios({
    //     method: 'post',
    //     url: 'https://sberid.odigital.site/sendCode',
    //     data,
    // })
    //     .then((response) => {
    //         const { data, status } = response.data;

    //         switch (status) {
    //             case 'SUBMIT_SMS_CODE_SUCCESS':
    //                 smsSuccess.current.classList.remove('is-hidden');
    
    //                 if (data.isSberUser) {
    //                     getDataSberUser(true);
    //                 } else {
    //                     window.location.href = returnUrl;
    //                 } 
    //                 break;
    //             case 'SUBMIT_SMS_CODE_FAIL':
    //                 try {
    //                     smsInput.current.classList.add('is-error');
    //                     smsSubmit.current.classList.remove('is-hidden');
    //                     smsSubmitLoading.current.classList.add('is-hidden');
    //                 } catch {
    //                     // eslint-disable-next-line
    //                     console.error('Ошибка');
    //                 }
    //                 break;
    //             default:
    //                 // eslint-disable-next-line
    //                 console.error('Ошибка');
    //                 break;
    //         }
    //     })
    //     .catch((e) => {
    //         // eslint-disable-next-line
    //         console.error(e);

    //         try {
    //             smsInput.current.classList.add('is-error');
    //             smsSubmit.current.classList.remove('is-hidden');
    //             smsSubmitLoading.current.classList.add('is-hidden');
    //         } catch {
    //             // eslint-disable-next-line
    //             console.error('Ошибка');
    //         }
    //     });
};

const SubmitPhone = ({ phoneNumber, getDataSberUser, returnUrl, tempToken }) => (
    <div className="sign-in-sms">
        <p className="sign-in-sms__desc">
            Введите SMS-код:
        </p>
        <form className="sign-in-sms__form" name="submitSmsCode">
            <input 
                type="text" 
                className="sign-in-sms__input-phone" 
                placeholder="123456"
                onChange={(e) => correctSmsCode(e.target.value)}
                ref={smsInput}
            />
            <svg className="sign-in-sms__success is-hidden" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                ref={smsSuccess} >
                <path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/>
            </svg>
            <span className="sign-in-sms__error">Что-то пошло не так, попробуйте ещё раз</span>

            <button 
                type="button" 
                className="sign-in-sms__submit"
                onClick={() => submitSmsCode(phoneNumber, getDataSberUser, returnUrl, tempToken)}
                ref={smsSubmit}>
                Отправить
            </button>
            <button 
                type="button" 
                className="sign-in-sms__submit is-active is-hidden"
                ref={smsSubmitLoading}>
                <Spinner />
            </button>
        </form>
    </div>
);

export default SubmitPhone;