import React from 'react';

const Spinner = () => (
    <div className="spinner">
        <div className="loadingio-spinner-ellipsis-7dorm2m2a2">
            <div className="ldio-unvncehvo79">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
);

export default Spinner;
