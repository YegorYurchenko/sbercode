<div align="center">

# Sberbank ID
</div>

### Общая информация

При работе со сборкой использовалась [Node.js](https://nodejs.org/en/download/) версии [11.15.0](https://nodejs.org/download/release/v11.15.0/)

Зависимости управляются пакетным менеджером [npm](https://www.npmjs.com/)

### Установка

1. Через консоль зайти в каталог клонированного проекта
1. Выполнить команду `npm install`, которая автоматически установит все зависимости, указанные в `package.json`
1. После окончания установки зависимостей в корне проекта появится каталог `node_modules`

### Запуск

1. `npm start` - запуск сборки в режиме разработки. После запуска сборки заработает локальный сервер по адресу `http://localhost:3000`
1. `npm run build`- создаст итоговую версию проекта для production
1. Пример Url для корректной работы: http://localhost:3000/#/sber-id&returnUrl=https://yandex.ru/&tempToken=efe26b

### Структура проекта

* `src` - исходники
  * `components` - компоненты проекта
  * `data` - json-заглушки для ajax-запросов
  * `images` - картинки
  * `pages` - страницы проекта
  * `styles` - стили
  * `index.js` - стартовый js-файл
* `public`
  * `index.html` - html-основа
* `build` - собранный проект
